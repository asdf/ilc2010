UNAME := $(shell uname)
ifeq ($(UNAME),Darwin)
XPDF = open
endif
XPDF ?= evince
# xpdf -z page
ALLTEX := $(wildcard *.tex *.cls *.sty *.bib *.bst)
SRC := main.tex ${ALLTEX}
OBJ := *.aux *.log *.bbl *.blg talk-outline.tex *~ *.out
FINAL := main.pdf talk-outline.pdf

view-talk: talk-outline.pdf
	${XPDF} $<

view: main.pdf
	${XPDF} $<

main.pdf: ${SRC}
	pdflatex main
	bibtex main
	pdflatex main
	pdflatex main

clean: semiclean
	-rm -f ${FINAL}

semiclean:
	-rm -f ${OBJ}

# Temporary address here:
#   http://common-lisp.net/project/asdf/ilc2010draft.pdf
website: main.pdf
	rsync main.pdf common-lisp.net:/project/asdf/public_html/ilc2010draft.pdf

# I have not the faintest idea how to check this for errors, etc.
talk-outline.pdf: talk-outline.org
	emacs --batch -q \
	      --eval '(find-file "talk-outline.org")' \
              --eval '(org-export-as-pdf ?l)' \
	      --eval '(save-buffers-kill-terminal)'

.PHONY: clean semiclean view website
