\section{{\ASDF}}
\label{sec:how-asdf-works}

\subsection{What {\ASDF} does}
\label{sec:what-asdf-does}

As a preliminary to a discussion of our work on {\ASDFii},
this section explains the role and function of {\ASDF}.
Readers unfamiliar with {\ASDF} may thus get
a sense of what this piece of software is about.
Even experienced {\ASDF} users may learn about
some of the subtleties of {\ASDF} that they might have to deal with.

In the {\CL} tradition, the unit of software organization
is called a {\em system}.
{\ASDF} allows developers to define systems in a declarative way,
and enables users to load these systems into the current {\CL} image.
{\ASDF} processes system definitions into objects
according to a model implemented using the {\longCLOS} ({\CLOS}).
This model is exposed to programmers and extensible by programmers,
who may thereby adapt {\ASDF} to their needs.

For most {\ASDF} users, the service that {\ASDF} provides is
the \lisp{asdf:load-system} function.
When {\ASDF} is  properly configured,
\lisp{(asdf:load-system "foo")} will load the system named \texttt{"foo"} into the current image;
{\ASDF} will first compile the system if necessary,
and it will recurse into declared dependencies
to ensure they are compiled and loaded.

To that end, it will take the following steps:
%\begin{itemize}
%\item
  (1) find the definition of the system \texttt{foo} in the {\ASDF} registry;
%\item
  (2) develop a plan to compile and load the system \texttt{foo},
  first recursing into all its declared dependencies, and
  then including all of its components;
%\item
  (3) execute this plan.
%\end{itemize}

Most developers load and modify existing systems.
Advanced developers define their own systems.
More advanced developers extend {\ASDF}'s object model:
they may define new {\em components} in addition to Lisp source files,
such as C source files or protocol buffer definitions;
or they may define new {\em operations} in addition to compiling and loading,
such as documentation generation or regression testing.

{\ASDF} is a central piece of software for the {\CL} community.
{\CL} libraries --- especially the open source libraries ---
are overwhelmingly delivered with {\ASDF} system descriptions, and
overwhelmingly assume that {\ASDF} will be present to handle
their own library dependencies.

One thing {\ASDF} does \emph{not} do is
download such library dependencies
when they are missing. % from the configured source code registry.
Other {\CL} programs tackle this problem,
while delegating build and load management to {\ASDF}.
These programs include
the obsolescent \lisp{asdf-install}, the popular \lisp{clbuild},
the up-and-coming \lisp{qui\-ck\-lisp}, and challengers like
\lisp{desire}, \lisp{LibCL} or \lisp{repo-install}.
% {\ASDF} also does not manage bugs (use launchpad),
% connect people (use IRC),
% solve version incompatibility issues (use LibCL),
% make coffee (use a coffee machine),
% or find you a sexy mate (use OkCupid), etc.


\subsection{Analogy with {\large \make}}
\label{sec:analogy-with-make}

It is conventional to explain {\ASDF} as the {\CL} analog of {\make}~\cite{Feldman79}:
both are used to build software, compile documentation, run tests.
However, the analogy is very limited:
while both {\ASDF} and {\make} are used for such high-level building tasks,
they differ in their goals, their design,
the constraints they respect, their internal architecture,
the concepts that underlie them, and the interfaces they expose to users.

{\ASDF} finds systems and loads systems,
problems that are not handled
by build tools such as {\make} or {\ant}.
These other build tools don't search for systems;
they must be pointed at a system definition
in the current or specified directory.
Finding systems at build time would be analogous
to a subset of \texttt{libtool};
finding and loading systems at runtime would correspond to
a subset of the Unix dynamic linker \texttt{ld.so}.
As for loading,
the fact that {\ASDF} is available for interactive use
makes it analogous to some component of the operating system
shell.\footnote{McDermott also makes this point in the paper about his chunk
  maintenance system~\cite{mcdermott05}.}
Loading some systems might thus be similar to
importing shell functions, starting a daemon,
registering a plugin in your browser,
loading code into some master process, etc.
Finally, {\ASDF} maintains state
in a running (perhaps long-running) Lisp image.
If system components have been changed, it is {\ASDF}'s job,
at the user's request, to generate and execute plans
to modify previously-loaded systems in order to accommodate those changes.

{\ASDF} also differs from {\make}
in terms of how systems are specified.
{\make} is built around a complex combination of
multiple layers of languages --- some domain-specific languages and
some generalized programming languages.
{\make} interprets a \emph{makefile} in the following way:
A text-sub\-sti\-tu\-ti\-on preprocessor expands macro definitions in the file,
producing a set of pattern-matching rules.
An inference engine uses the rules,
chaining backwards from the targets it has been directed to build.
The rules are annotated with parameterized shell scripts
that actually perform the build actions.

{\make} is a powerful tool that can express arbitrary programs,
but makefiles can grow into a mesh of code that defies any simple analysis.
{\ASDF} is a small {\CL} program,
and its system definitions are data rather than programs.

Supporting a new file type is relatively easy with {\make},
by adding a new rule with an appropriate pattern to recognize
filenames with the conventional extension,
and corresponding shell commands.
Supporting a new file type in {\ASDF} is more involved,
requiring one to define a class and a few methods
as extensions to the {\ASDF} protocol.
Unfortunately, this procedure is complicated by the fact that
the {\ASDF} protocol is poorly documented.

On the other hand, when building C programs with {\make},
arbitrary side-effects have to be specified
in a different language, in the shell layer of the makefile.
When building {\CL} programs with {\ASDF},
arbitrary side-effects are specified in Lisp itself
inside the components being built.
This is because the C compiler and linker
are pure functional file-to-file transformers,
whereas the {\CL} compiler and loader are imperative languages.
The ability Lisp has to do everything without cross-language barriers
is a conceptual simplification, but of course, it doesn't save you from
the \emph{intrinsic} complexity of such side-effects.

\hide{
\rtof{I don't believe the following helps the reader a lot, and our purpose is only
  to explain make to the extent that it helps understand ASDF... [2010/08/23:rpg]}
On the one hand, this means that {\ASDF} itself
is much less powerful than {\make}, and
offers very restricted system definition language.
On the other hand, {\ASDF} is part of a whole
that is just as powerful as {\make},
and the strictures of system definitions allow for
semantically richer analysis and manipulation of such,
with tools such as automatic packagers.
}

A final difference between {\ASDF} and {\make}
is that {\ASDF} generates a full plan of the actions required
to fulfill all the dependencies of its goal
before performing the planned actions.
By contrast, {\make} performs actions
as it traverses its dependency tree~\cite{AITR-874,Feldman79}.
We will discuss this further in Section~\ref{sec:plan-generation}.


\subsection{Basic {\ASDF} object model}
\label{sec:asdf-object-model}

\begin{figure}[t]
  \centering
  \begin{minipage}{0.95\columnwidth}
\small
\begin{verbatim}
(defsystem "hello-lisp"
  :description "hello-lisp: a sample Lisp system."
  :version "0.3"
  :author "Joe User <joe@example.com>"
  :licence "Public Domain"
  :depends-on (foo-utils)
  :components ((:file "packages")
               (:file "macros"
                :depends-on ("packages"))
               (:file "classes"
                :depends-on ("packages"))
               (:file "methods"
                :depends-on ("macros" "classes"))
               (:module "main"
                :depends-on ("macros")
                :serial t
                :components
                ((:file "hello")
                 (:file "goodbye")))))
\end{verbatim}
  \end{minipage}
  \caption{Sample {\ASDF} system definition.}
  \label{fig:sampleASD}
\end{figure}

Figure \ref{fig:sampleASD} shows a sample {\ASDF} system definition,
illustrating core features of the {\defsystem} macro.
This form shows how a system can be defined
in terms of components (files and sub-modules).
It also shows how system dependencies are specified
(\lisp{hello-lisp depends-on foo-utils}),
and gives example metadata (authors, versioning, license, etc.).

The {\defsystem} macro parses the system definition into
a set of linked objects, all of them instances of (subclasses of) {\component}.
The main object will be of type {\system}.
Primitive {\component}s of systems are typically
instances of a subclass of \lisp{source-file},
by default \lisp{cl-source-file}.
There are other predefined component types,
including \lisp{static-file},
representing files that are distributed with a system,
but which do not undergo operations.
Systems can be recursively organized in a tree of {\module}s
that may or may not map to a similar tree of directories.

System management tasks consist of applying the generic function
{\operate} on parameters specifying an {\operation} and a {\system}.
The {\operation} is either a {\loadOp},
specifying that a system or component is to be loaded into the current image,
or a {\compileOp}
specifying that a system or component is to be compiled into the filesystem.
The {\system} is designated by a string or a symbol, and
{\operate} will first find and load the thus named system definition.
This is done by the generic function \lisp{find-system}, which searches
for the system definition in a configurable system registry
(and its in-memory cache).
The system definition search is one of the aspects of {\ASDFi} that we reformed;
see Section \ref{sec:input-locations}.

After a system definition is found,
{\operate} generates a \emph{plan} for completing the {\operation}
using the function {\traverse} described below.
The plan will be a list of {\step}s;
%each {\step} a pair of an \emph{operation} and a \emph{component};
the list will be topologically sorted such that
all the dependencies of a {\step} appear before that {\step}.
If a plan is successfully generated,
{\operate} will {\perform} each of its {\step}s in sequence.
If a {\step} fails, {\ASDF} offers the user a chance to
correct the {\step}, retry, and continue according to plan,
making it easy to fix simple mistakes without interrupting the operation.


\subsection{Plan generation}
\label{sec:plan-generation}

The plan-generating function {\traverse}
performs a depth-first, postorder traversal
over the {\step}s needed to operate on the target system and its dependencies.
Each {\step} is a pair of an {\operation} and a {\component},
What we mean by postorder here is that
when applying an {\operation} to a {\system} (or {\module}),
the plan will contain {\step}s
first to complete the {\operation} to the sub-components,
then to perform it on the {\system} (or {\module}) itself.
For example, a {\loadOp} plan
for the system in Figure~\ref{fig:sampleASD}
is given as Figure~\ref{fig:samplePlan}.
Note the lines marked with a \dag,
which show that operations on composite components ({\module}s and {\system}s)
are scheduled after operations on their components.

\begin{figure}[t]

  \begin{minipage}{1.0\columnwidth}
\begin{alltt}
 ((#<COMPILE-OP> . #<CL-SOURCE-FILE "packages">)
  (#<LOAD-OP> . #<CL-SOURCE-FILE "packages">)
  (#<COMPILE-OP> . #<CL-SOURCE-FILE "macros">)
  (#<COMPILE-OP> . #<CL-SOURCE-FILE "classes">)
  (#<LOAD-OP> . #<CL-SOURCE-FILE "macros">)
  (#<LOAD-OP> . #<CL-SOURCE-FILE "classes">)
  (#<COMPILE-OP> . #<CL-SOURCE-FILE "methods">)
  (#<COMPILE-OP> . #<CL-SOURCE-FILE "hello">)
  (#<LOAD-OP> . #<CL-SOURCE-FILE "hello">)
  (#<COMPILE-OP> . #<CL-SOURCE-FILE "goodbye">)
\dag (#<COMPILE-OP> . #<MODULE "main">)
  (#<COMPILE-OP> . #<SYSTEM "hello-lisp">)
  (#<LOAD-OP> . #<CL-SOURCE-FILE "methods">)
  (#<LOAD-OP> . #<CL-SOURCE-FILE "goodbye">)
\dag (#<LOAD-OP> . #<MODULE "main">)
\dag (#<LOAD-OP> . #<SYSTEM "hello-lisp">))
\end{alltt}
  \end{minipage}
  \centering

  \caption{Sample {\loadOp} plan for the system definition in
    Figure~\ref{fig:sampleASD}.
    We assume that \lisp{foo-utils} is already compiled and loaded.}
  \label{fig:samplePlan}
\end{figure}

When building a plan, {\traverse} skips {\step}s
it can prove are not necessary.
A {\step} is necessary if it hasn't been done yet
or if it is \emph{forced} by a change to one of the component's dependencies.
When a \emph{compilation} {\step} is necessary,
all the steps that depend on it are \emph{forced} to be necessary,
since whatever was done before will be out of date
by the time the {\step} is performed.
If a {\step} and all the compilation {\step}s transitively
required in order to complete it
have already been done (by a previous run of {\ASDF}),
then the {\step} is not necessary and can be skipped.

To determine whether a {\step} has already been done,
{\traverse} calls the generic function {\opdonep}
on the {\operation} and {\component}.
For a compilation {\step},
wanted for its effects on the filesystem,
{\opdonep} compares the timestamps of
the corresponding \lisp{input-files} and \lisp{output-files}
(if present).
For a load {\step}, wanted for its effects on the current Lisp image,
there are no \lisp{output-files}, and
{\opdonep} compares the timestamp of the \lisp{input-files}
to the time they were last loaded into the current image (if ever).
{\Step}s with neither \lisp{input-files} nor \lisp{output-files}
(e.g., where the component is a {\module} or {\system})
are never considered necessary unless forced by a dependency or sub-component.
\hide{\footnote{
{\defsystem} internally expands
the specification of {\dependsOn} relationships between components
into two kinds of dependencies between related {\step}s:
{\inOrderTo} constraints that force {\step}s that depend on them,
and {\doFirst} constraints that do not.
Consider some component {\xa} that {\dependsOn} a component {\xb},
both being the default component type a \lisp{cl-source-file}.
Before {\xa} is either compiled (via operation {\compileOp})
or loaded (via operation {\loadOp}),
{\xb} must first be loaded in the current image,
which itself depends on {\xb} having been compiled.
However, merely having to load {\xb} will not by itself
force the (re)compilation of {\xa},
as compiling {\xa} from an equivalent set of loaded components
is expected to yield an equivalent result.
On the other hand, having to (re)compile {\xb} will
force the (re)compilation of {\xa}.
Therefore, {\traverse} first issues \lisp{in-order-to} dependencies,
and checks {\opdonep} status, and
if the operation needs to be done on the component,
recurses into {\doFirst} dependencies.
}}

% \fixme{There's a discussion of in-order-to on gmane, apparently.
%   \url{http://article.gmane.org/gmane.lisp.cclan.general/674}.  Check this
%   before calling this done.}

Tracing {\traverse} and inspecting the plan it returns
is often a good way to debug one's system definitions.
Running {\traverse} can also be useful as an introspection tool on a system
(e.g. to determine what files need be recompiled,
and run according tests after recompilation).
We have improved {\traverse} somewhat in {\ASDFii},
but have limited the scope of our efforts
out of concern for backwards compatibility.
See Section \ref{sec:traverse-bug-fix}.
{\traverse} is not an exported part of the {\ASDF} API,
so in theory users and extenders should not depend on it,
but that has proven not to work,
because of the leakiness of the abstraction.
We discuss this further in Section \ref{sec:future-directions}.

Importantly, note that system definitions only specify
dependencies between {\step}s.
These dependencies only specify a partial order,
so for a given operation and system
there may be multiple possible complete plans.
Moreover, the semantics do not dictate
a ``plan-then-execute'' implementation.
And indeed, a parallelizing extension to {\ASDF}, {\POIU},
does things differently (see Section \ref{poiu}).


\hide{
\ftor{
  If some points of semantics are too complex, vague or ambiguous
  for the human mind to fully understand,
  then we may consider them as unspecified, and
  safe that no one in his right mind will rely on them.
  I some insane person did rely on such unspecified semantics,
  yet not contribute suitable documentation to {\ASDF} itself,
  we can call them foul on it
  especially after several maintainers (at least you and I)
  expressed concern about such points of semantics.
  Let's not be overly afraid here.
  Conservative is one thing, neurotic about change is another,
  especially when large-scale automated testing robots are on the horizon.
  }

\ftor{
  To a first approximation, the plan in {\make} is much more concrete
  and almost directly visible to the user:
  it's just the list of rules \texttt{foo: bar ; command}
  that are specified after expansion
  of of the text-substituting macros. OK, it's slightly more complex than that, since
  there are ``implicit rules'' with wildcard like
  \texttt{\%.fasl: \%.lisp ; lisp -compile \$<}
  and which rule applies depends on the specificity and/or order of the rules
  (see make info section 10.8 Implicit Rule Search Algorithm);
  the model of {\make} while in a sense more concrete and low-level than that of {\ASDF},
  nevertheless possesses some limited level abstraction that {\ASDF} does not have,
  sufficient to write rules to handle building of all types of files:
  compiling C into objects, linking objects into executables, etc.
  {\ASDF} necessitates extensions in a different language (Lisp),
  which is often awkward.
}

\rtof{
  I don't believe that's correct.  The rules in the makefile are not the
  plan --- they are the pieces out of which a plan is assembled (if {\make}
  plans; I am not convinced this is the case).  The parallel is the system definition in
  {\ASDF}.  That is not a plan; that is what the plan is assembled out of.  For
  example, if I ask {\ASDF} to do a {\loadOp} on a system, it will first build a
  plan, an ordered sequence of operation - component pairs.  \emph{Then} it will
  execute that plan.  I don't believe that {\make} does anything like this.  In
  fact, I don't understand how make actually does its job.  Similarly, I can
  have a bunch of rules about how to build the pdf of the ASDF doc from texinfo, but those
  rules will not be part of the plan if I ask make to build the html version of
  the doc.

The plan is an intermediate product of the ASDF operation process.

The following is the paragraph that I think needs to be supported technically
or cut.}

The way {\ASDF} builds its plan --- and determines which actions are necessary ---
is another point of difference between {\make} and {\ASDF}.
{\make} builds its plan without querying the filesystem for
timestamps or the existence of files,
and does all these filesystem queries dynamically as the plan unfolds.
{\ASDF} queries the filesystem for timestamps once, in advance, while planning.
This difference has no visible consequence to the user in the common simple case;
but if a source file is generated by some of the {\step}s it depends on,
this will confuse {\ASDF} as it fails to find a timestamp at planning time.
The main benefit for {\make} is that it will work correctly when building things
in parallel (options \texttt{-j} or \texttt{-l} of {\make}).
}

%\newpage{} % for aesthetics reason, given the layout

\subsection{Social role of {\ASDF}}
\label{sec:social-role-asdf}

{\ASDF} had a major role in enabling
the growth of open source {\CL} libraries,
and the renaissance of the {\CL} community.
Prior to the existence of {\ASDF},
there have been many efforts at consolidating
a Lisp community~\cite{WatersSurvival},
but it was hard to assemble the pieces of code
gathered from over the net, because
there was no portable way to specify how to build a system
out of components that were not all supplied together.
Installing a {\CL} system was often a tricky matter
of filling a load file full of logical or physical pathname definitions.
Sharing a {\CL} system across a group, even on the same filesystem,
often involved fussy coordination of logical pathname definitions.
Typically, {\CL} software developers simply developed and delivered
enormous, monolithic systems, and there was little opportunity
for code reuse across work groups.

{\ASDF} changed all this
by providing a \textit{de facto} standard for specifying systems.
{\ASDF} made it easy to specify how one system could depend on others.
This could now be done simply by naming the systems depended on,
and users could trust {\ASDF}
to find the requisite
system definitions and system contents.
Unfortunately, {\ASDF} had several flaws that made this system configuration,
and the social software development process, more difficult than need be
--- though still much easier than doing without {\ASDF}.
In this paper, we will discuss some of these flaws,
and the steps we have taken to overcome them in {\ASDFii}.

The {\CL} renaissance is still fragile.
For that reason, and because of central role of {\ASDF} in the community,
we needed to be extremely careful that our modifications and extensions to {\ASDF}
would only strengthen it and not, for example,
break the community up into islands
running separate and incompatible {\ASDF} versions.


% \draft{ Side note:  where did the notion of ``protocol,'' which we see in,
%   e.g.,. Keene's book and the CLIM spec, come from?}

% Some keen new ideas:
% \begin{itemize}
% \item
%   Use \lisp{*load-truename*} to avoid the annoying need for library
%   \emph{installers} to specify the location of their source files.
% \item
%   Use symbolic links to allow the system definition files to
%   \emph{appear to} be in arbitrary locations
%   while still enabling ASDF to find the source files.
%   (Still supported by ASDF2, but ASDF2 now also offers
%   recursive search in directory hierarchies).
% \item
%   Make extensive use of CLOS (after \cite{Pitman-Large-Systems}),
%   notably define generic functions for core parts of the protocols,
%   and use CLOS classes and object to model the components and operations.
% \end{itemize}

% You need to understand the following about the protocol that ASDF follows,
% in order to understand the rest of the paper:

%   ASDF will try to load a system definition
%   when requested to \lisp{operate} on a system
%   that either it does not know,
%   or where the system definition has changed.
%   This is the job of \lisp{asdf:find-system}.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
