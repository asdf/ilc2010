\section{Dynamic Code Update}

\label{sec:upgradeability}

\subsection{Social Rationale}

As discussed in the introduction, in-memory upgradeability of {\ASDF}
was essential to solve the social issues regarding
the development, distribution and usage of new versions of {\ASDF}.
Only if we guarantee that {\ASDF} can be upgraded if needed
can users rely on new features and bug fixes of {\ASDF}.

Previously, there was no portable way to load and configure {\ASDF}
unless it had been pre-loaded with your Lisp image
(as by \texttt{common-lisp-controller} under Debian),
and there was no way to upgrade a pre-loaded {\ASDF} with a new version.
With {\ASDFii}, users can install a new version of {\ASDF}
into an image with an old version simply by executing the following command,
assuming {\ASDF} was properly configured to find the new definition:
\begin{quotation}
  \lisp{(asdf:load-system :asdf)}
\end{quotation}

In an apparent paradox,
\moneyquote{allowing for divergence creates an incentive towards convergence}:
because they are confident that {\ASDFii} can be upgraded,
implementation vendors have less pressure to
be conservative and keep a ``trusted'' old version
(which may differ between implementations).
Also, with less pressure to get it exactly right,
CL implementers need not hesitate to upgrade to the latest upstream release;
if they update their code every so often
and {\ASDF} does not change too fast,
they will quickly converge to the same version, the latest stable release.
By removing the requirement for {\it a priori} coupling of release cycles,
we achieve better {\it a posteriori} coupling of release cycles.


\subsection{Technical Challenge}

Unlike other build systems, such as {\make},
\moneyquote{{\ASDF} is an ``in-image'' build system
managing systems that are compiled and loaded in the current {\CL} image}.

Build tools of other languages typically rely
on some external, operating system provided shell
to build software that is loaded into virtual machines
({\em processes} in Unix parlance) distinct from the current one.
When using these other languages, all the state necessary to build software
is typically kept in the filesystem,
and incompatible changes in interfaces or internals of the build system
are resolved simply by starting a new virtual machine.

{\ASDF} does not start separate processes for compilation.
We believe that there are a number of reasons for this design decision.
%\begin{itemize}
%\item
First,
  {\CL} implementations historically have run on
  a vast variety of operating systems,
  some of which lacked the capability of virtualizing a Lisp process.
  Previous build systems were designed around that constraint,
  and {\ASDF} followed their design.
%\item
Also,
  even on modern operating systems that allow this virtualization,
  starting a {\CL} process can sometimes be a relatively expensive process
  (depending on the individual {\CL} implementation).
% \item
Moreover,
  because of the presence of code to be executed at compile time and
  the dependence on a substantial amount of compile-time state,
  it is difficult to decompose the process of building a {\CL} system
  into independent pieces and parcel them out to different processes.
%\item
Finally, {\CL} programmers often build very extensive state
  in a long-living {\CL} image, and so prefer to keep them alive.
  {\ASDF} supports such a use pattern.
%\end{itemize}

Because {\ASDF} performs its build tasks in the user's current Lisp process,
upgrading {\ASDF} entails modifying some existing
functions and data structures {\it in situ},
requiring delicate surgery
to keep things working as you modify them.

To make things slightly harder, the same source code must be able
to both define a fresh {\ASDF} (if it hasn't been loaded yet),
or upgrade an existing {\ASDF} installation to the current code
(if a previous version already exists).
In addition, the code for an {\ASDF} version must recognize
the special case when the very same version is already loaded
so as to ensure such reloads are idempotent.
It does this by relying on a simple version identification string,
to be bumped up at every modification of {\ASDF}.


\subsection{Rebinding a symbol}

The semantics of redefining or overriding a function
is not fully specified by the {\CL} standard.
The many implementations at the time of standardization
may have had explicitly different semantics,
the semantic difficulties may have been overlooked,
implementers may have called for underspecification
as leaving them more room for optimization,
or it may have otherwise not been considered appropriate
for the committee to standardize a practice that wasn't widely accepted.
In writing the code that makes it possible to upgrade {\ASDF},
we encountered two complementary difficulties
when rebinding the functional value of symbols.

The first difficulty arises
from incompatibilities between new and old function definitions
bound to a same symbol
when new functions are dynamically called by an old client,
with data following the old convention.
The second difficulty arises
from incompatibilities between the new and old function definitions
bound to a same symbol
when old functions are statically called by an old client,
with data following the new convention.

A \emph{dynamic call} is when the call site dereferences
the function bound to the symbol at runtime, and does so at every call.
A \emph{static call} is when the compiler or linker dereferences
the function bound to the symbol once at compile-time or load-time
and prepares the runtime to always directly use the code of that function.
An actual implementation need not do the above naively,
as long as it behaves in a semantically equivalent way.\footnote{
  For instance, assuming functions are seldom rebound,
  dynamic calls may be implemented just like static calls,
  except that the value of the binding is a cache that gets invalidated
  between the time the function is rebound and the time the cache is next used.

  As for static calls, they may be implemented
  not just by linking a call to the proper code value,
  but also by inlining the body of the target function,
  or at the other end of the spectrum,
  by doing a dynamic call to an alpha-converted symbol
  that will never be rebound.
}

The two above difficulties are inherent in redefining functions
and are not specific to either {\CL} or {\ASDF}.
However, these difficulties are particularly relevant in the case of {\ASDF},
because it drives compilation and loading of Lisp code
possibly including new versions of {\ASDF} itself.
{\ASDF}'s redefined functions are therefore likely to be in the continuation
of their own function redefinitions,
where the old code will for a short while
be a client to the new code.
Moreover, these difficulties are compounded by the fact that
the {\CL} standard~\cite[section 3.2.2.3]{ANSI:1996:ANSa} does not specify
whether any particular call will be dynamic or static,
unless the function was explicitly declared \lisp{notinline},
in which case it should always be dynamic.
In practice, implementations may legitimately
inline function bodies,
cache effective methods for generic function calls,
specialize call sites to declared calling conventions, etc.
This means that redefining a function requires taking proper precautions
against errors that may or may not happen,
depending on details of individual {\CL} implementations,
the nature of the function being redefined,
and the evaluation context.

% \ftor{
%   Or do some SB-PCL optimization sometimes trigger
%   \emph{illegitimate} static method cache semantics for notinline gfs?
%   --- TODO: write checks for method caching of notinline gfs,
%   and collect results from several implementations with cl-launch.}

{\CL} provides a primitive {\fmakunbound} that is meant to
undo the binding of a symbol to a function.
{\fmakunbound} ought to clear out any related state
and make way for a new definition.
Indeed, in the simple case where a function is
not referenced in the continuation of the current compile or load,
and not exported to code from other files,
all references to it will be overridden by newly loaded code.
In this case, it is sufficient to {\fmakunbound} the function symbol
(and possibly re-declaim its type) before redefining it
with an incompatible signature.
Any inlined or cached version of the function will be overridden
by the new definitions in the newly loaded file.

On the other hand, when a function may be referenced in the continuation
of the current compile or load, then
whether {\fmakunbound} will work or not
depends on the implementation, the optimization levels, etc.
Generic functions in particular may require special MOP operations
that haven't been standardized.
We cannot work around limitations of MOP standardizations
by using a portability layer such as {\CLOSERMOP}~\cite{costanza:closer},
lest by doing so we create a circular dependency
between the portability layer (loaded using {\ASDF}) and {\ASDF} itself.


\subsection{Shadowing a symbol}

In cases where {\fmakunbound} will not work
and a symbol cannot be rebound,
we may instead {\unintern} the symbol whose function binding we want to redefine.
Doing so we effectively {\em shadow} the old definition.
All existing references to the symbol from old code will continue
to point to the old symbol and its existing bindings.
% \footnote{
% Of course if he insists the programmer may explicitly use reflection
% to query the system's latest run-time symbol by same same if any.
% }
The next time same symbol name appears, a new, distinct, symbol will be interned,
and further code that is read into the system
will refer to this new symbol and its associated bindings.

In addition to cases where bugs and limitations of the implementation
prevent {\fmakunbound} from working,
there are cases where the {\CL} standard doesn't provide
any redefinition mechanism, and
shadowing the old symbol is the only option available.
This is notably the case regarding
{\CL} not providing any guaranteed primitive
to undo the declaration making the binding of a symbol to a variable
{\em special} (i.e. dynamic, as opposed to lexical, i.e. static),
or {\em constant}, or to undo declarations using a named type
that is being redefined.
These issues are usually worked around by programmers following
syntactic conventions,
such as \lisp{*ear-muffs*} for special variables
and something similar for \lisp{+constants+}.
There should \emph{never} be a need to turn the \lisp{*ear-muffs*} variable into
something that is lexically scoped, or to change \lisp{+constants+} at all.

The main downside of shadowing as a redefinition mechanism is that it requires
that all clients be reloaded and possibly recompiled
to be able to use the new interface,
even if the code of these clients hasn't changed at all.
Indeed, previously loaded clients will continue to use
the old symbols and their bindings,
and there may be confusion if old and new clients interact
while expecting to be talking to the same service.

Another problem is that {\unintern} runs the risk of causing ``collateral damage.''
When a symbol has several bindings associated to it,
such as a function or macro; variable or constant; type or class or condition;
property, etc.
All of these bindings will simultaneously become inaccessible
when the symbol is uninterned.

Consider a user developing
on a {\CL} image that includes {\ASDF}
and on top of it {\ASDF} clients such as {\POIU} or CFFI-GROVEL.
If the user upgrades {\ASDF},
then she must also reload {\POIU} and CFFI-GROVEL before she may use them.
She must do so even if there were no code changes
in either of the latter systems,
lest the previously loaded system be in an invalid, unusable state.
Code in these systems may be
linked to obsolete, now-uninterned symbols from the old {\ASDF}.
For these client systems
to function properly, they must be linked against
the symbols from the new {\ASDF}.

Ideally, whether we rebind or shadow would be a matter of
the distinction between intension and extension:
which symbols we consider intensional fixed entry points
that denote some ``same'' higher meaning when implementation changes underneath,
and which symbols denote extensional constant code values,
the implementation of which cannot be changed, but that can be shadowed and forgotten.

In practice, which of rebinding and shadowing we use depends
on the implementation into which we are loading {\ASDF},
because different implementations have different
quirks, bugs and constraints when upgrading code.
For instance, rebinding some generic functions
fails to flush method caches on SBCL,
but shadowing symbols while loading a FASL
breaks linker optimizations on ECL and GCL.
We barely managed to support the basic use case of upgrading
from implementation-provided versions of {\ASDF} to the latest version of {\ASDF},
but we had to deal with more hurdles
than we expect the average {\CL} programmer ever to want to deal with.

To make things more complex, in {\CL},
multiple symbols of the same name may coexist,
if each of them is interned in a different package.
Packages are global flat structures,
and each package may import symbols from other packages (without renaming).
Packages may ``use'' other packages that export symbols,
which helps automate some of this importing.
If {\unintern} was causing problems when redefining functions,
redefining packages only leads to more madness as uninterned symbols
may have been imported by other packages,
the package \lisp{use} graph may have changed, etc.
There again, we may try either to do complex surgery on an existing package object
and preserve its relationship to other packages using it or its symbols,
or to simply rename it away and shadow it and all its symbols with it,
and require client packages to be reloaded to link to the new package object.

{\ASDFii} takes care to define the \lisp{ASDF} package if it doesn't exist,
redefine it properly if it exists, etc.
{\ASDFii} reuses existing packages and symbols
whenever possible, so as not to invalidate previously interned client code, etc.
This package wrangling was difficult to get right, and
once again, we have to take into account the eager linking done by ECL and GCL.
One reason we could make this package wrangling work
is that we do not need to blindly handle the general case
of upgrading arbitrary package definitions to arbitrary new ones.
All we needed to do was to upgrade previous versions of our own packages.
This was simplified by the fact that
our package does not \lisp{use} any other package that is a moving target.
If any package \lisp{use}s \lisp{ASDF} and that somehow causes a clash,
it is the responsibility of the authors of that client package
to update their code.


\subsection{Happy non-issue: Data Upgrade}
\label{sec:happy-non-issue}

\moneyquote{{\CL} makes dynamic data upgrade extraordinarily easy.}
Classes can be redefined,
slots can be added to them, removed from them, or modified,
and all instances will be automatically updated before their next use
to fit the new definition.
The {\longCLOS} ({\CLOS}) \cite{bobrow_etal88}
allows users to control this instance update programmatically
by defining methods on {\uifrc}.
We rely on this functionality in {\ASDFii}
to ensure that previously loaded systems can still be used after an upgrade of {\ASDF}
(assuming they are not themselves {\ASDF} extensions).
Without such functionality, preserving the existing data (system definitions, etc.)
would have been a major undertaking requiring a global rewrite,
and requiring much better control over the version of {\ASDFi} being replaced
than was easily available.

There was one catch with using {\uifrc} for the purposes of {\ASDFii}.
This was a chicken-and-egg issue between {\defclass} and {\defmethod} {\uifrc}:
it was difficult to order the definitions
so that the same code would work without warning or error
in both the case of defining a fresh {\ASDF}
and the case of an upgrade from a previous {\ASDF}.

Defining the class before the method may on some implementations
cause objects to be upgraded before the method is defined
and therefore without the upgrade being properly run.
Defining the method before the class in the source code
may cause a warning the first time around when the class isn't defined yet.
Inserting an introspective check for class existence
may cause the method definition not to be statically compiled
and emit a warning on some implementations.
Protecting the method definition with delayed evaluation (as we finally did)
hushes the warning.
Unfortunately, it also
causes slightly inefficient runtime compilation on some implementations.
Nevertheless,
it doesn't cause any significant user-visible pause,
since the user is compiling {\ASDF} and (presumably lots of other code with it);
the slight added delay is not perceptible.

The {\CL} protocol for class redefinition is relatively well-de\-sig\-ned
and quite effectively handles the difficult problem of schema upgrade
that other programming languages do not dare to tackle.
However, the schema upgrade
API was written with ``upgrade scripts'' in mind,
and is clumsy to use when writing code that specifies end-result semantics
independently of whether the code is an initial definition or an upgrade.

Note that none of this would have been possible
if {\ASDF}, like its predecessor {\mkdefsys},
had been using pre-{\CLOS} \lisp{defstruct}.
{\CL} structures do not provide a safe upgrade
protocol the way {\CLOS} classes do.

\subsection{Towards a better specification}

It is to the credit of {\CL} that dynamic code upgrade is possible at all;
it is not possible in most programming languages.
However, it is possible to support dynamic code upgrade much better.
For instance, Erlang solves the issue of dynamic code upgrade
by providing syntactic distinction
between the two semantically different kinds of calls:
calls specifying a syntactically unqualified identifier
are always semantically static calls (to a function in the same module),
and calls to a syntactically module-qualified identifier (even in the same module)
are always semantically dynamic calls.
The effect of function redefinition on each call site
is therefore perfectly predictable,
and this does not prevent optimizations
as the {\CL} standard authors might have feared.

It is probably possible to express the Erlang semantics on top of {\CL},
by explicitly \lisp{funcall}ing either
\lisp{(fdefinition 'foo)} or \lisp{(load-time-value (fdefinition 'foo))}
depending on the call being static or dynamic.
However this is cumbersome, non-idiomatic, and most importantly
requires existing code to be modified to use the new convention
before it may be safely upgraded.
Therefore it is not compatible with using existing libraries as black boxes.
Future Lisp standards and specifications could learn from Erlang.

{\CL} users could incorporate Erlang-like semantics
in a most\-ly transparent way
by layering a {\CL} implementation on top of {\CL},
shadowing the usual reader and evaluator to replace them with something
that provides well-defined semantics for hot upgrade,
assuming all code is (re)compiled on top of this implementation
rather than directly with the underlying implementation.
This, however, would be a large, challenging task and not obviously worth the cost.
Furthermore,
if one were to design and implement
what amounts to a new language on top of {\CL},
would it and should it be {\CL} all again?
Interestingly, in the presence of concurrent threads
within a same Lisp image (as is common nowadays),
some model of atomicity or PCLSRing \cite{PCLSRing} would be required,
which also goes beyond the current {\CL} language specification.

Lacking such a better-specified Lisp, possibly implemented atop {\CL},
there are ways to work around these limitations;
but not only are they are quite unidiomatic,
they require manual management.
For instance, we could use some kind of symbol versioning:
use completely different symbols any time we would previously redefine things,
mark old symbols as obsolete and never reuse them.
In other words, add a monotonicity (purity) constraint to our bindings
and achieve guaranteed static calls through {\em manual} alpha-conversion.
Therefore function \lisp{FOO} would never be redefined,
instead \lisp{FOO-V2} then \lisp{FOO-V3}
would be defined and used by client code.
This client code in turn
would itself need to be renamed with a new version
since its contents have changed to use new function names.
In a limited way, that is what uninterning symbols does for you,
and what renaming away packages would do, etc.
However, monotonicity requires new clients
not only to be recompiled, but also to be modified
any time any code is changed incompatibly.

This latter approach is semantically safe and technically simple,
but we didn't adopt it, because of its social implications.
This approach
requires us to either keep supporting old interfaces,
or gratuitously break old programs, all the more gratuitously
when the incompatibility with previous interface lies in
``extensions'' that were conceptually broken and remained (mostly?) unused.
Its advantage is that it allows to make interfaces formal where they weren't.
Its disadvantage is that it requires to maintain formal interfaces
where you mostly don't need them.
As part of the {\CL} community, that ascribes a high cost
to social interactions and especially to formality in such,
we chose to tackle one annoying technical issue that we needed only solve once
over a social issue that would crop up every time.
% Also, ``I object to doing things that computers can do.'' --- Olin Shivers


\subsection{Lessons Learned}

We encountered many technical issues when providing
hot upgradability of {\ASDF}.
The good news is that it is possible to write hot upgradable code in {\CL}
in a reasonably portable way, whereas dynamic code upgrade is not even possible
in most programming languages.
The bad news is that hot upgrade remains quite tricky,
especially \emph{portable} hot upgrade,
and it imposes limitations on the code to be upgraded.
In order to write hot upgrade code,
you have to use application-specific knowledge to determine what is safe and
what is not.
Furthermore,
not only is such dynamic code upgrade not thread safe,
it can even damage the operation of a single-threaded environment.

\moneyquote{{\CL} support for hot upgrade of code may exist
but is anything but seamless.}
Happily, programmers only need
to deal with hot upgrade as an issue for \emph{their own} programs, and so
they have the required, application-specific knowledge available;
so at least the problem is socially solvable, if technically hard.

In the end,
\moneyquote{the general problem with {\CL} is that
its semantics are defined in terms of irreversible side-effects
to global data structures in the current image.}
Not only does this complicate hot upgrade,
it also makes semantic analysis, separate compilation, dependency management,
and a lot of things much harder than they should be.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
