#lang at-exp slideshow
@(begin
;;; Slides for ILC talk about ASDF2, October 2010
;;; Time budget = 30 minutes of presentation time.
;;; Should be not more than 15 slides (not counting the outlines).
;; http://www.common-lisp.net/project/asdf/ilc2010-presentation.rkt
;; This document is available under the bugroff license.
;; _bugroff: http://tunes.org/legalese/bugroff.html

(define *width* 1024)
(define *height* 768) ;700

(set-margin! 20)

;(require scheme/runtime-path)
;(define-runtime-path bad-bg-path "badbg.png")
;(define bad-background (bitmap bad-bg-path))

(require scheme/gui/base)
(require slideshow/code)

(define ~ @t{     })

(define (spacing* l (space ~))
  (cond
    ((null? l) l)
    ((pair? l) (append (list space)
                       (if (pair? (car l)) (car l) (list (car l)))
                       (spacing* (cdr l))))
    (else (error 'spacing*))))

(define (spacing l) (cdr (spacing* l)))

(define *blue* (make-object color% "blue"))
(define (url x) (colorize (tt x) *blue*))

(define xlide
  (make-keyword-procedure
   (lambda (kw kv . l)
     (keyword-apply slide kw kv (spacing l)))))

(define outline
  (let ([sub-para (lambda l
                    (para #:width (* 3/4 (current-para-width)) l))])
    (make-outline
     'nutshell "Part I: What is ASDF?"
     #f

     'two "Part II: Hot-patching ASDF"
     (lambda (tag)
       (vl-append
        (sub-para "Why is this critical?")
        (sub-para "Why was it hard?")
        (sub-para "How did we manage?")))

     'three "Part III: Configuration"
     (lambda (tag)
       (vl-append
        (sub-para "Finding input systems")
        (sub-para "Finding compiled files")))

     'four "Part IV: Best practices"
     #f

     'five "Part V: Lessons learned"
     #f

     'six "Part VI: Future directions"
     #f

     'end "Conclusion"
     #f)))

;; FIXME: Get title to agree with paper title
(slide
   #:title "ASDF 2"
   @bt{Building Common Lisp software}
   @bt{made slightly easier}
   ~ ~
   (para #:align 'center @t{François-René Rideau,} @it{ITA Software})
   (para #:align 'center @t{Robert P. Goldman,} @it{SIFT, LLC})

   @t{International Lisp Conference, October, 2010}
   ;;@url{http://fare.tunes.org/computing/asdf2.ss}
   )

(outline 'nutshell)

(xlide
 #:title "Summary"
 @para{"What were we doing?"}
 @item{"Fixing up ASDF"}
 @item{"Trying not to wreck key piece of CL community plumbing"}
 @para{"We discovered"}
 @item{"Interesting technical challenges from hot-patching"}
 @item{"Interesting social challenges"}
 @para{"Some principles"}
 @item{"Don't wreck backward compatibility"}
 @item{"Let users configure based on what they know and library authors configure based on what " @it{"they"} " know."}
 )

(outline 'one)

(xlide
 #:title "ASDF"
 @para{@it{De facto} standard for building CL software}
 @para{Kind of @tt{make} for CL hackers}
 @para{Written by Daniel Barlow in 2001}
 @para{In the line of older Lisp @tt{defsystem}})

(slide
 #:title "Fodder from previous presentation."
)

(xlide
 #:title "ASDF 2: incremental improvements"
 @para{compatible with ASDF, minus bugs}
 @para{more portable pathname behaviour}
 @para{has better configurability}
 @para{better supports Windows, ABCL, GCL, etc.})

(xlide
 #:title "The same, just better"
 @para{Age-old bugs fixed: module dependencies}
 @para{Performance bugs (traversal from N^3 to N)}
 @para{Better, updated documentation}
 @para{All tests now pass everywhere (except GCL)})

(xlide
 #:title "More portable pathname behavior"
 @para{@tt{(:file "foo/bar")}}
 @para{@tt{(:static-file "foo/baz.quux")}}
 @para{@tt{(:module "src/quux" ...)}}
 @para{@tt{(:module "frob" :pathname "" ...)}})

(xlide
 #:title "Better Configurability: Source-registry"
 @para{DSL to replace *central-registry*}
 @para{taken from Lisp, environment, config files}
 @para{sensible defaults}
 @para{WIN: decouples build and distribution})

(xlide
 #:title "Better Configurability: Output translations"
 @para{DSL to replace A-B-L, C-L-C, CL-Launch}
 @para{taken from Lisp, environment, config files}
 @para{sensible defaults: on by default}
 @para{WIN: allows multi-implementation setups})

(xlide
 #:title "Better support for..."
 @para{Impl'n: ABCL, ECL, CMUCL, CCL, GCL...}
 @para{Windows: shortcuts, output-translations...}
 @para{Extensions: POIU, C-L-C, CL-Launch}
 @para{WIN: Upgradability, decouples release})

(xlide
 #:title "Whodunnit"
 @para{Mainly: fare, rpgoldman, juanjo, janderson}
 @para{Most lines were touched or added.}
 @para{Size has tripled, mostly due to config DSLs})

(xlide
 #:title "What's next?"
 @para{ASDF 3: more declarative extensions}
 @para{comprehensive tests allow backward incompatibility}
 @para{XCVB: enforce dependencies})

(xlide
 #:title "Share and enjoy!"
 @para{@tt{http://common-lisp.net/project/asdf/}}
 @para{@tt{#+asdf2}}
 @para{RSN: bundled with your fav' implementation}
 @para{Already in ABCL, CCL, ECL...})

(slide
 #:title "End of fodder from previous presentation."
)

(outline 'two)



(outline 'three)



(outline 'four)



(outline 'five)


(outline 'six)


(outline 'end)

);end


;;; Local Variables:
;;; mode: scheme
;;; compile-command: mred
;;; End:
